//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = " "
    
  //Question 1 here
  //Working Varriables 
let numbers = [54,-16,80,55,-74,73,26,5,-34,-73,19,63,-55,-61,65,-14,-19,-51,-17,-25];
let positiveOdd = [];
let negativeEven = [];

     for (let i = 0; i < numbers.length; i++) 
     {
         if ((numbers[i] % 2 === 1) && (numbers[i]>0))
        {
         positiveOdd.push(numbers[i]);
        }      
        else if ((numbers[i] % 2 === 0) && (numbers[i] < 0))
        {
          negativeEven.push(numbers[i]);
        } 
     }
    
 //Display the output
      output = "Question 1\n\n" + 'Positive Odd: ' +positiveOdd + "\n" + 'Negative Even: ' +negativeEven;
      let outPutArea = document.getElementById("outputArea1")
      outPutArea.innerText = output
}
 
function question2(){
    let output = " "

var one,two,three,four,five,six;

 one = 0;
 two = 0;
 three = 0;
 four = 0;
 five = 0;
 six = 0;

for(var i = 0; i<60000; i++) {
	var randomN = Math.floor((Math.random() * 6) + 1)
	if (randomN === 1){
		one++
	} else if (randomN === 2 ){
		two++
	} else if (randomN === 3 ){
		three++
	} else if (randomN === 4){
		four++
	} else if (randomN === 5){
		five++
		
	} else (randomN === 6)
		six++
	
};
 
        output =  "Question 2\n\n" + "Frequency of die rolls\n" + "1:" + one + "\n" + "2:" + two + "\n"  + "3:" + three + "\n"  + "4:" + four + "\n" + "5:" + five + "\n"  + "6:" + six; 
        let outPutArea = document.getElementById("outputArea2") 
        outPutArea.innerText =  output
}
function question3(){
    let output = " "
    
    //Question 3 here
   
let frequencies = [0,0,0,0,0,0,0];
let i = 1; 
    
for(i=0; i<60000; i++){	
	let value = Math.floor((Math.random() * 6) + 1);
	frequencies[value]++;
}
 
     output = "Question 3\n\n" + "Frequency of die rolls\n"+"1:" + frequencies[1] + "\n" +" 2:"+frequencies[2] + "\n" + " 3:" + frequencies[3]+ "\n" + " 4:" + frequencies[4]+ "\n" + " 5:" + frequencies[5] + "\n" + " 6:" + frequencies[6];  
     let outPutArea = document.getElementById("outputArea3") 
     outPutArea.innerText = output
    
}

function question4(){
    let output = " "
        
        //Question 4 here
        
let value,x
let dieRolls = {
frequencies: [0, 0, 0, 0, 0, 0, 0],
total:60000,
exceptions: []
}

for (let i =1 ; i < dieRolls.total ; i++){
	
	 value = Math.floor((Math.random() * 6) + 1);
	 
	dieRolls.frequencies[value]++;
   
}

            
x=0;
for (let j=1 ; j<dieRolls.frequencies.length; j++){
	
	if (dieRolls.frequencies[j]>10100 || dieRolls.frequencies[j]<9900) {
		dieRolls.exceptions[x]=(j);
	}
	x=x+1;
}
 
     output =  "Question 4\n\n" + "Frequency of dice rolls\n" + "Total rolls:" + dieRolls.total + "\n" + "Frequencies:" + "\n" + "1: " + dieRolls.frequencies[1] + "\n" + "2: " + dieRolls.frequencies[2] + "\n" + "3: " + dieRolls.frequencies[3] + "\n" + "4: " +dieRolls.frequencies[4] + "\n" + "5: " + dieRolls.frequencies[5] + "\n" + "6: " + dieRolls.frequencies[6] + "\n" + "Exceptions: " +  dieRolls.exceptions;
    let outPutArea = document.getElementById("outputArea4")
    outPutArea.innerText = output
    
}

function question5(){
    let output = " "
    
  //Question 5 here 
  //Creating an object to store information
	let person = {
			name: "Jane",
			income: 127050
		}

//Working varriables
let tax = 0;

if (person.income > 18200 && person.income <= 37000)
{
	tax = (person.income - 18200) * 0.19;
}
else if (person.income > 37000 && person.income <= 90000)
{
	tax = ((person.income - 37000) * 0.325) + 3572;
}
else if (person.income > 90000 && person.income <= 180000)
{
	tax = ((person.income - 90000) * 0.37) + 20797;
}
else if (person.income > 180000)
{
	tax = ((person.income - 180000) * 0.45) + 54097;
}
else
{
	tax = 0;
}

//Display the output
    output = "Question 5\n\n" + person.name + "'s income is " + person.income + ", and her tax owed is: $" + tax.toFixed(2);
    let outPutArea = document.getElementById("outputArea5")
    outPutArea.innerText = output
}